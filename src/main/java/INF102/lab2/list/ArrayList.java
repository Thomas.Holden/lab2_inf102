package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) {
		return (T) elements[index];
	}
	
	@Override
	public void add(int index, T element) {

		if (n == elements.length) {
			increaseSize();
		}
		while (index > elements.length) {
			increaseSize();
		}
		if (elements[index] != null) {
			for (int i = index; i < elements.length - 1; i++) {
				elements[i + 1] = elements[i];
			}
		}


		elements[index] = element;
		n++;
	}

	private void increaseSize() {
		int oldLength = elements.length;
		Object clonedElements[] = new Object[oldLength + 1];
		for (int i = 0; i < oldLength; i++) {
			clonedElements[i] = elements[i];
		}
		elements = clonedElements;
	}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}